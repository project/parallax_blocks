<?php

namespace Drupal\parallax_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Parallax block.
 *
 * @Block(
 *   id = "parallax_block",
 *   admin_label = @Translation("Parallax Block")
 * )
 */
class ParallaxBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $menus = [
      'top'       => $this->t('Top menu'),
      'secondary' => $this->t('Secondary menu'),
      'footer'    => $this->t('Footer menu'),
      'bottom'    => $this->t('Bottom menu'),
    ];

    $default = 'top';

    if (isset($this->configuration['menu'])) {
      $default = $this->configuration['menu'];
    }

    $form['menu'] = [
      '#type'          => 'radios',
      '#multiple'      => FALSE,
      '#options'       => $menus,
      '#default_value' => $default,
      '#description'   => $this->t('Select menu to display in this block'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['menu'] = $form_state->getValue('menu');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $menu_name = $this->configuration['menu'];

    switch ($menu_name) {
      case 'top':
        $menu = $this->menuManager->getTopMenu();
      break;
      case 'secondary':
        $menu = $this->menuManager->getSecondaryMenu();
        break;
      case 'footer':
        $menu = $this->menuManager->getFooterMenu();
        break;
      case 'bottom':
        $menu = $this->menuManager->getFooterBottomMenu();
        break;
      default:
        $menu = '';
        break;
    }

    return $menu;
  }

}
