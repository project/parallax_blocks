Parallax blocks

{
  "type": "package",
  "package": {
    "version": "v2.0.5",
    "name": "scrollmagic/scrollmagic",
    "type": "drupal-library",
    "source": {
      "url": "https://github.com/janpaepke/ScrollMagic.git",
      "type": "git",
      "reference": "v2.0.5"
    },
    "dist": {
      "url": "https://github.com/janpaepke/ScrollMagic/archive/v2.0.5.zip",
      "type": "zip"
    }

  }
},
{
  "type": "package",
  "package": {
    "version": "v1.20.4",
    "name": "greensock/greensock",
    "type": "drupal-library",
    "source": {
      "url": "https://github.com/greensock/GreenSock-JS.git",
      "type": "git",
      "reference": "v1.20.4"
    },
    "dist": {
      "url": "https://github.com/greensock/GreenSock-JS/archive/1.20.4.zip",
      "type": "zip"
    }

  }
}